import numpy as np
import cv2
import csv

def naming_convention(index, mode):
    if mode == "in":
        return ("Images/IM_" + str(index + 1) + ".jpg")
    if mode == "out":
        return ("Output_Images/IM_" + str(index + 1) + "_output.jpg")
    if mode == "csv":
        return (naming_convention(index, "in")[:-4])

def show_image(image):
    cv2.imshow('view', image)
    cv2.waitKey(0)
    cv2.destroyWindow('view')

# settings
numberOfPictures = 8

dp = 1.5
minimumDistanceBetweenCircles = 10
minimumRadius = 50
maximumRadius = 480

imageSize = 640,480
maximumOffset = 80

colourTuple = (119, 206, 97)
colourTupleCentralCirclesList = [(int(0xF0), int(0xE4), int(0x44)),
                                 (int(0x44), int(0xF0), int(0xE4)),
                                 (int(0xE4), int(0x44), int(0xF0))]

thresholds = 20

outputArray = []    # output, follows format [table radius, concrete radius, ratio]
for i in range(numberOfPictures):
    # open image
    image = cv2.imread(naming_convention(i, "in"))
    imageCopy = image.copy()

    # perform edge detection
    greyscaleImage = cv2.cvtColor(imageCopy, cv2.COLOR_BGR2GRAY)    # convert image to greyscale

    edgeSumImage = np.zeros((np.shape(greyscaleImage)), np.uint8)
    for i_threhsold in range(thresholds):
        dummy, thresholdImage = cv2.threshold(greyscaleImage, (255/(thresholds - 1))*i_threhsold, 255, cv2.THRESH_BINARY)
        edgesImage = cv2.Canny(thresholdImage, 100, 200)
        edgesImage = edgesImage / (thresholds - 1)
        edgeSumImage += edgesImage.astype(np.uint8)
        #show_image(edgesImage)

    #show_image(edgeSumImage)
    edgesImage = cv2.Canny(edgeSumImage, 60, 120)
    #show_image(edgesImage)
    #cv2.imwrite("Output_Images/edge_detection.jpg", edgesImage)

    # find circles within image and cull those that deviate from the centre more than 'maximumOffset'
    circles = cv2.HoughCircles(edgesImage, cv2.HOUGH_GRADIENT, dp, minimumDistanceBetweenCircles, minRadius=minimumRadius, maxRadius=maximumRadius)
    if circles is not None:
        circles = np.round(circles[0,:]).astype("int")
        toDelete = []
        index = 0
        for (x, y, r) in circles:
            offset = (((x - (imageSize[0]/2))**2) + ((y - (imageSize[1]/2))**2))**0.5
            if offset < maximumOffset: cv2.circle(imageCopy, (x,y), r, colourTuple, 1)
            else: toDelete.append(index)
            index += 1
    else:
        print("No circles found in picture", i)
        continue

    # delete circles that deviate from centre too much
    circles = np.delete(circles, toDelete, 0)

    #cv2.imwrite("Output_Images/initial_circle_image.jpg", imageCopy)
    #print(np.shape(circles))

    # find largest circle
    maximumIndex = np.argmax(circles[:,2])
    maximumCircle = circles[maximumIndex]
    imageCopy = image.copy()
    cv2.circle(imageCopy, (maximumCircle[0], maximumCircle[1]), maximumCircle[2], colourTuple, 2)
    #cv2.imwrite("Output_Images/maximum_radius_image.jpg", imageCopy)

    # find circle closest to centre of largest
    circleArray = np.delete(circles, maximumIndex, 0)
    #print(np.shape(circleArray))
    offsetArray = (((circleArray[:,0] - maximumCircle[0])**2) + ((circleArray[:,1] - maximumCircle[1])**2))**0.5

    circleArray = circleArray[np.argsort(offsetArray)]     # sort by circles by sorted offset array
    offsetArray = offsetArray[np.argsort(offsetArray)]     # sort offsets to match
    for i_ in range(3):
        centralCircle = circleArray[i_]
        cv2.circle(imageCopy, (centralCircle[0], centralCircle[1]), centralCircle[2], colourTupleCentralCirclesList[i_ % 3], 2)

    # create display image
    background = np.ones((np.shape(imageCopy)[0], np.shape(imageCopy)[1] + 500, 3), np.uint8) * 255
    background[:np.shape(imageCopy)[0], :np.shape(imageCopy)[1], :] = imageCopy
    for i_ in range(3):
        cv2.putText(background, str(i_ + 1) + ":", (np.shape(imageCopy)[1] + 10, 30 + (i_ * 40)),
                    cv2.FONT_HERSHEY_COMPLEX, 1, (0,0,0), 0)
        cv2.rectangle(background, (np.shape(imageCopy)[1] + 70, 10 + (i_ * 40)), (np.shape(imageCopy)[1] + 110, 40 + (i_ * 40)),
                      colourTupleCentralCirclesList[i_], thickness=-1)
    cv2.putText(background, "SPACE: Only table", (np.shape(imageCopy)[1] + 10, 30 + (120)),
                cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 0), 0)
    cv2.putText(background, "        defined correctly", (np.shape(imageCopy)[1] + 10, 30 + (160)),
                cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 0), 0)
    cv2.putText(background, "ANY KEY: Table incorrectly", (np.shape(imageCopy)[1] + 10, 30 + (200)),
                cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 0), 0)
    cv2.putText(background, "           defined", (np.shape(imageCopy)[1] + 10, 30 + (240)),
                cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 0), 0)
    #show_image(background)

    # determine circle and gather results
    cv2.imshow('Display', background)
    key = cv2.waitKey(0)
    cv2.destroyWindow('Display')
    if key == ord('1'):
        centralRadius = circleArray[0][2]
    elif key == ord('2'):
        centralRadius = circleArray[1][2]
    elif key == ord('3'):
        centralRadius = circleArray[2][2]
    elif key == ord(' '):
        centralRadius = "N/A"
        correctTableSize = True
    else:
        centralRadius = "N/A"
        correctTableSize = False

    # save image to be checked later
    if centralRadius != "N/A": outputArray.append([maximumCircle[2], centralRadius, centralRadius/maximumCircle[2]])
    else:
        if correctTableSize: outputArray.append([maximumCircle[2],centralRadius, centralRadius])
        else: outputArray.append([centralRadius,centralRadius,centralRadius])
    cv2.imwrite(naming_convention(i, "out"), imageCopy)


#print(np.shape(outputArray))
output = open("Results.csv", "w", newline='')
writer = csv.writer(output)
writer.writerow(["This is a csv file of your results"])
writer.writerow(["Image Name", "Table Radius (pixels)", "Concrete Radius (pixels)", "Ratio (CR/TR)"])
for i in range(len(outputArray)):
    dummyOut = [naming_convention(i, "csv"), outputArray[i][0], outputArray[i][1], outputArray[i][2]]
    writer.writerow(dummyOut)
output.close()