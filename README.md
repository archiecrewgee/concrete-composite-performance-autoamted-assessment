# Concrete Composite Performance Automated Assessment

The following is a short script written to assist in a civil engineering research project. This project investigates the effective of additives on the mix's spread distance over time. I make software so lord knows I don't actually understand it but my friend was needlessly manually measuring pixel distances on images she'd taken so there I was to help one rainy day.

The program assess the given images and uses the ratio of distance between the known measurment plate and concrete trapazoid to give a diameter of this spread. The results are stored in a CSV file. Some example images from the actual experiment are included as part of the project for testing purposes; the complete image set was analyised via this tool by my mate on her own PC.


## Setup

To setup ensure python is installed then, from the top level of the repository run:

 1. Build virtual environment: `python -m venv .venv`
 2. Install requirements: `.\.venv\Scripts\python.exe -m pip install -r requirements.txt`
 3. Run the application: `.\.venv\Scripts\python.exe .\Program\main.py`


## Perform your own alaysis

No idea why anyone would use such a shoddy program except desperate, hungover civil engineering students but to do so just add your pictures to the top level `Images` directory with the same naming format as the examples. Then re-run the program.